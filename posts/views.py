from django.views.generic import ListView
from .models import Post
from .forms import PostForm
from django.shortcuts import render, redirect


def index(request):
    posts = Post.objects.all()
    return render(request, 'pages/index.html', {'title': 'Home page', 'posts': posts[:3]})


def about(request):
    return render(request, 'pages/about.html', {'title': 'About page'})


def article(request):
    return render(request, 'pages/article.html', {'title': 'Articles'})


def create(request):
    error = ''
    if request.method == 'POST':
        if_form = PostForm(request.POST)
        if if_form.is_valid():
            if_form.save()
            return redirect('home')
        else:
            error = 'bad form entering'
    create_form = PostForm()
    context = {
        'form': create_form,
        'error': error,
    }
    return render(request, 'pages/create.html', context)
