from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='home'),
    path('about/', views.about, name='about'),
    path('article/', views.article, name='article'),
    path('create/', views.create, name='create'),
]
