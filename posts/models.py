from django.db import models


class Post(models.Model):
    title = models.CharField('title_name', max_length=50)
    text = models.TextField('content')

    def __str__(self):
        return self.text[:15]


